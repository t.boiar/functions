const getSum = (str1, str2) => {
	let sum = 0;
  if(typeof str1 === 'string' && typeof str2 === 'string') {
		str1 = str1 === '' ? 0 : str1;
		str2 = str2 === '' ? 0 : str2;

		if(!isNaN(Number(str1)) && !isNaN(Number(str2))) {
			sum = BigInt(str1) + BigInt(str2);
			return String(sum);
		}
	}
  return false;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
	let comments = 0;
	for(let post of listOfPosts) {
		if(post.author === authorName) {
			posts += 1;
		}
		if(post.hasOwnProperty('comments')) {
			for(let comment of post.comments) {
				if(comment.author === authorName) {
					comments += 1;
				}	
			}
		}
	}
  return `Post:${posts},comments:${comments}`;
};

const tickets = (people) => {
  const twentyFives = [];
	const fifties = [];
	const hundreds = [];
	for(let bill of people) {
		if(bill == 25) {
			twentyFives.push(bill);
		} else if(bill == 50) {
			if(twentyFives.length) {
				twentyFives.pop()
			} else {
				return 'NO';
			}
			fifties.push(bill);
		} else {
			if(twentyFives.length && fifties.length) {
				fifties.pop();
				twentyFives.pop();
			} else if(twentyFives.length >= 3) {
				twentyFives.length = twentyFives.length - 3;
			} else {
				return 'NO';
			}
			hundreds.push(bill);
		}
	}
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
